<?php

namespace App\Controller\Admin;

use App\Entity\Grade;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class GradeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Grade::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('value'),
            DateField::new('created'),
            AssociationField::new('discipline'),
            AssociationField::new('teacher'),
            AssociationField::new('student'),
        ];
    }
}

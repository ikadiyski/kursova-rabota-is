<?php

namespace App\Controller\Admin;

use App\Entity\Discipline;
use App\Entity\Grade;
use App\Entity\Student;
use App\Entity\Teacher;
use App\Repository\DisciplineRepository;
use App\Repository\StudentRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /** @var StudentRepository */
    private $studentRepository;

    /** @var DisciplineRepository */
    private $disciplineRepository;

    public function __construct(StudentRepository $studentRepository, DisciplineRepository $disciplineRepository)
    {
        $this->studentRepository = $studentRepository;
        $this->disciplineRepository = $disciplineRepository;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $students = $this->studentRepository->findAll();

        $total_grades = 0;
        $count_grades = 0;

        foreach ($students as $student) {
            $grades = $student->getGrades();
            $count_grades += $grades->count();
            foreach ($grades as $grade) {
                $total_grades += $grade->getValue();
            }
        }

        $top_10_students = $this->studentRepository->createQueryBuilder('s')
        ->orderBy('s.averageGrade', 'DESC')
        ->setMaxResults(10)
        ->getQuery()
        ->getResult()
        ;

        $average_grades_per_discipline = [];

        foreach ($this->disciplineRepository->findAll() as $discipline) {
            $total = 0;
            foreach ($discipline->getGrades() as $grade) {
                $total += $grade->getValue();
            }

            $average_grades_per_discipline[] = [
                'discipline' => $discipline,
                'average' => $total ? number_format($total / $discipline->getGrades()->count(), 2) : 0,
            ];
        }

        $average_grades = $total_grades ? number_format($total_grades / $count_grades, 2) : '-';

        return $this->render('admin/dashboard.html.twig', [
            'average_grades' => $average_grades,
            'top_10_students' => $top_10_students,
            'average_grades_per_discipline' => $average_grades_per_discipline
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('изпитно състояние на студенти от КСТ');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Начало', 'fa fa-home');
        yield MenuItem::linkToCrud('Disciplines', 'fas fa-list', Discipline::class);
        yield MenuItem::linkToCrud('Teachers', 'fas fa-list', Teacher::class);
        yield MenuItem::linkToCrud('Students', 'fas fa-list', Student::class);
        yield MenuItem::linkToCrud('Grades', 'fas fa-list', Grade::class);
    }
}

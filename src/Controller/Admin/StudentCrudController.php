<?php

namespace App\Controller\Admin;

use App\Entity\Student;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class StudentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Student::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            // set this option if you prefer the page content to span the entire
            // browser width, instead of the default design which sets a max width
            ->renderContentMaximized()
            ->setSearchFields(['firstName', 'email', 'speciality', 'semester', 'facultyNumber'])
            ;
    }


    public function configureFields(string $pageName): iterable
    {
        if (Crud::PAGE_INDEX === $pageName) {
            return [
                NumberField::new('getAverageGradeCalculated', 'Average'),
                TextField::new('firstName'),
                TextField::new('secondName'),
                TextField::new('lastName'),
                TextField::new('email'),
                TextField::new('city'),
                TextField::new('phone'),
                TextField::new('speciality'),
                NumberField::new('semester'),
                TextField::new('studentType'),
                TextField::new('facultyNumber'),
            ];
        }

        return parent::configureFields($pageName);
    }
}
